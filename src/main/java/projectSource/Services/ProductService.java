package projectSource.Services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.boot.autoconfigure.kafka.KafkaProperties.Producer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import projectSource.Enities.Products;



@Service
@Transactional
public class ProductService {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Products> findAll() {

		/*
		 * return
		 * entityManager.createNativeQuery("SELECT * FROM tbl_Employee ").getResultList(
		 * );
		 */

		/*
		 * Table nameTbl = Employee.class.getAnnotation(Table.class);
		 * 
		 * return entityManager.createNativeQuery("SELECT * FROM " +
		 * nameTbl.name()).getResultList();
		 */
		 return entityManager.createQuery("Select t from " + Products.class.getSimpleName() + " t").getResultList();
	}
	public void delete(Products enity) {
		entityManager.remove(enity);

	}

	public void deleteById(int id) {
		Products enity = entityManager.find(Products.class, id);
		entityManager.remove(enity);
	}

	public Products getId(int id) {

		return entityManager.find(Products.class, id);
	}

	public void add(Products enity) {
		this.entityManager.persist(enity);
	}

	public Products update(Products enity) {
		return entityManager.merge(enity);
	}
}
