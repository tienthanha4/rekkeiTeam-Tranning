package projectSource.Services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import projectSource.Enities.Categories;

@Service
@Transactional
public class CategoriesSevice {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Categories> findAll() {

		/*
		 * return
		 * entityManager.createNativeQuery("SELECT * FROM tbl_Employee ").getResultList(
		 * );
		 */

		/*
		 * Table nameTbl = Employee.class.getAnnotation(Table.class);
		 * 
		 * return entityManager.createNativeQuery("SELECT * FROM " +
		 * nameTbl.name()).getResultList();
		 */
		 return entityManager.createQuery("Select t from " + Categories.class.getSimpleName() + " t").getResultList();
	}
	public void delete(Categories enity) {
		entityManager.remove(enity);

	}

	public void deleteById(int id) {
		Categories enity = entityManager.find(Categories.class, id);
		entityManager.remove(enity);
	}

	public Categories getId(int id) {

		return entityManager.find(Categories.class, id);
	}

	public void add(Categories enity) {
		this.entityManager.persist(enity);
	}

	public Categories update(Categories enity) {
		return entityManager.merge(enity);
	}

}
