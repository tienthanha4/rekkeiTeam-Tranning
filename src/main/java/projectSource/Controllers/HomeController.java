package projectSource.Controllers;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


import projectSource.Services.CategoriesSevice;
import projectSource.Services.ProductService;



@Controller
public class HomeController extends BaseController {
	@Autowired
	private ProductService productService;
	@Autowired
	private CategoriesSevice categoriesSevice;
	

	
	@GetMapping("/Admin")
	public String getAdmin() {
	
		return "Admin";
	}
	
	@GetMapping("/Admin/findAll")
	public String getFindAll(Model model) {
		
		/*
		 * for (int i = 0; i < 5; i++) { Products product = new Products();
		 * product.setTitle("banh ran "+i); product.setDetail_description("ngon");
		 * product.setShort_description("ngon ma");
		 * product.setCategories(categoriesSevice.findAll().get(0));
		 * product.setPrice(BigDecimal.valueOf(20)); productService.add(product); }
		 */	
		
		model.addAttribute("products", productService.findAll());
		return getAdmin();
	}
	@GetMapping("/findAll")
	public String getFindAll2(Model model) {
		
		/*
		 * for (int i = 0; i < 5; i++) { Products product = new Products();
		 * product.setTitle("banh ran "+i); product.setDetail_description("ngon");
		 * product.setShort_description("ngon ma");
		 * product.setCategories(categoriesSevice.findAll().get(0));
		 * product.setPrice(BigDecimal.valueOf(20)); productService.add(product); }
		 */	
		
		model.addAttribute("products", productService.findAll());
		return getAdmin();
	}
	@GetMapping(value = "/Admin/AddProduct")
	public String viewAddProduct() {
		return "AddProduct";
	}
	@GetMapping("/login") 
    public String getLogin() {
		try {
			System.out.println(getUserLogined().getAuthorities()+"  "+getUserLogined());
			
		} catch (Exception e) {
			System.out.println("cdcscsdcds");
		}
		
        return "login";
    }
	@GetMapping("/home")
    public String index() {
        return "index";
    }
	@GetMapping("/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null) {
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
	    return "redirect:/";
	}


}
