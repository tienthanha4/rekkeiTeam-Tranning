package projectSource.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import projectSource.DTO.MyFirstApiDto;
import projectSource.Services.MyFirstApiServe;

@RestController
public class MyFirstApiColltroller {
	@GetMapping(value = "/Api/getStrings")
	public String getString() {
		return "My First Api";
	}
	
	@Autowired
	private MyFirstApiServe myFirstApiServe;
	
	@GetMapping(value = "/Api/service/getStrings")
	public String getStringService() {
		return myFirstApiServe.getString();
	}
	
	@GetMapping(value = "/Api/PostData")
	public List<MyFirstApiDto> getData (){
		List<MyFirstApiDto> myFirstApiDtos = new ArrayList<MyFirstApiDto>();
		for(int i = 0 ; i < 5 ; i++) {
			MyFirstApiDto myFirstApiDto  = new MyFirstApiDto();
			myFirstApiDto.setCode(""+i);
			myFirstApiDto.setName(""+i);
			myFirstApiDto.setAge(i);
			myFirstApiDtos.add(myFirstApiDto);
		}
		return myFirstApiDtos;
	}

}
