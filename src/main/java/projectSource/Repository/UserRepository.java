package projectSource.Repository;

import org.springframework.data.repository.CrudRepository;

import projectSource.Enities.User;




public interface UserRepository extends CrudRepository<User, Integer> {

    User findByEmail(String email);

}
