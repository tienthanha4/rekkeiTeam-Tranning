package projectSource.Repository;

import org.springframework.data.repository.CrudRepository;

import projectSource.Enities.Role;



public interface RoleRepository extends CrudRepository<Role, Integer> {

    Role findByName(String name);

}
