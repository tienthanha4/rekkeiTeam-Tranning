package taskBE1.controller;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import taskBE1.DTO.DTOMyFristApi;
import taskBE1.Service.MyFristApiService;

@RestController

public class MyFirstApiController {
	@Autowired
	private MyFristApiService myFristApiService;

	@GetMapping("/getString")
	public String getString() {
		return "Hello World";
	}

	@GetMapping("/getStringService")
	public String getStringService() {
		return myFristApiService.stringApi();
	}

	@GetMapping("/getJson")

	public List<DTOMyFristApi> getJson() {
		List<DTOMyFristApi> dtoMyFristApis = new ArrayList<DTOMyFristApi>();

		for (int i = 0; i < 6; i++) {
			DTOMyFristApi dtoMyFristApi = new DTOMyFristApi();

			dtoMyFristApi.setAge(i);
			dtoMyFristApi.setCode("code" + i);
			dtoMyFristApi.setName("name" + i);
			dtoMyFristApis.add(dtoMyFristApi);

		}

		return dtoMyFristApis;
	}

	/*
	 * #7 @RequestBody nhan kieu du lieu json
	 * 
	 */
	/*
	 * #8 output : code:nullname:nullage:null Numbers in JSON must be an integer or
	 * a floating point.
	 */
	
	@RequestMapping(value = "/postJson", method = RequestMethod.POST, consumes = "application/json")
	public String postJson(@RequestBody DTOMyFristApi dtoMyFristApi ) {
		
		return " code: " + dtoMyFristApi.getCode() + " name: " + dtoMyFristApi.getName() + " age: "
				+ dtoMyFristApi.getAge();
	}	
	/*#9
	 * tra ve du lieu duong ung vs cac truong 
	 *  @RequestParam nhan du lieu khi form data day du lieu lein url
	 */
	@RequestMapping(value = "/getJsonParam", method = RequestMethod.POST)
	public String getJsonParam(@RequestParam("code") String code, @RequestParam("name") String name,
			@RequestParam("age") Integer age) {

		return " code: " + code + " name: " + name + " age: " + age;
	}
	
	
	/*
	 * #10
	 * #10 http://localhost:8080/getJsonPathVal/code/name/1 kq code: code name: name
	 * age: 1
	 * 
	 */
	@RequestMapping(value = "/getJsonPathVal/{code}/{name}/{age}", method = RequestMethod.POST, consumes = "application/json")
	public String getJsonPathVal(@PathVariable("code") String code, @PathVariable("name") String name,
			@PathVariable("age") Integer age) {

		return " code: " + code + " name: " + name + " age: " + age;
	}
	
	/*
	 * #11 form data
	 */
	@RequestMapping(value = "/getData", method = RequestMethod.POST)
	public String getData(HttpServletRequest request) {
		DTOMyFristApi dtoMyFristApi = new DTOMyFristApi();
		dtoMyFristApi.setAge(Integer.parseInt(request.getParameter("age")));
		dtoMyFristApi.setCode(request.getParameter("code"));
		dtoMyFristApi.setName(request.getParameter("name"));
		
		return dtoMyFristApi.getName();
	}
	
	
	
	
	/*
	 * #12 file
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public byte[] submit(@RequestParam("file") MultipartFile file, ModelMap modelMap) throws IOException {

		return file.getInputStream().readAllBytes();
	}
	
	@RequestMapping(value = "/putJson/{age}", method = RequestMethod.PUT, consumes = "application/json")
	public List<DTOMyFristApi> getJson(@PathVariable("age") int age, @RequestBody DTOMyFristApi dtoMyFristApi) {
		List<DTOMyFristApi> dtoMyFristApis = new ArrayList<DTOMyFristApi>();
		 
		dtoMyFristApis.addAll(getJson());
		for (DTOMyFristApi dto : dtoMyFristApis) {

			if (dto.getAge() == age) {
				dtoMyFristApis.remove(dto);
				dto.setAge(dtoMyFristApi.getAge());
				dto.setCode(dtoMyFristApi.getCode());
				dto.setName(dtoMyFristApi.getName());
				dtoMyFristApis.add(dto);

				break;

			}

		}

		return dtoMyFristApis;
	}
	
	/*
	 * #7 err :
	 * 
	 * 
	 * #8 output : code:nullname:nullage:null Numbers in JSON must be an integer or
	 * a floating point.
	 * 
	 * #9 loi vi requestParam day du lieu thong qua url
	 * 
	 * #10 http://localhost:8080/getJsonPathVal/code/name/1 kq code: code name: name
	 * age: 1
	 * 
	 * #13 kq code: null name: null age: null
	 * 
	 * @RequestBody de anh xa du lieu tu clinet len server bo no thi khong nhan dc
	 * du lieu dc post len
	 * 
	 * #14
	 */

}
