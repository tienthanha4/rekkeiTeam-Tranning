package taskBE2.enities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="tbl_employee")
public class Employee {
	public Employee(Integer id, String code, String name, String email, String phone, Integer age) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.age = age;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	public Employee() {
		
		// TODO Auto-generated constructor stub
	}

	@Column(name = "code",length = 200,nullable = true)
	private String code;
	
	@Column(name = "name",length = 200,nullable = true)
	private String name;
	
	@Column(name = "email",length = 200,nullable = true)
	private String email;
	
	@Column(name = "phone",length = 200,nullable = true)
	private String phone;
	
	@Column(name = "age",nullable = true)
	private Integer age;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
}
