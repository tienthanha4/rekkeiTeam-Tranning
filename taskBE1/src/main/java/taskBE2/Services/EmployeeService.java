package taskBE2.Services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import taskBE2.DTO.EmploySeachDTO;
import taskBE2.enities.Employee;

@Service
@Transactional
public class EmployeeService {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Employee> findAll() {

		/*
		 * return
		 * entityManager.createNativeQuery("SELECT * FROM tbl_Employee ").getResultList(
		 * );
		 */

		/*
		 * Table nameTbl = Employee.class.getAnnotation(Table.class);
		 * 
		 * return entityManager.createNativeQuery("SELECT * FROM " +
		 * nameTbl.name()).getResultList();
		 */
		 return entityManager.createQuery("Select t from " + Employee.class.getSimpleName() + " t").getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Employee> Search(EmploySeachDTO searchModel) {
		Table nameTbl = Employee.class.getAnnotation(Table.class);
		String sql = "SELECT * FROM " + nameTbl.name() + " where 1 = 1 ";

		if (!StringUtils.isEmpty(searchModel.key)) {
			sql += " and (code like '%" + searchModel.key + "%' " + "or email like '%" + searchModel.key + "%' "
					+ "or phone like '%" + searchModel.key + "%' " + " or name like '%" + searchModel.key + "%')";
		}

		return entityManager.createNativeQuery(sql).getResultList();
	}

	public void delete(Employee enity) {
		entityManager.remove(enity);

	}

	public void deleteById(int id) {
		Employee enity = entityManager.find(Employee.class, id);
		entityManager.remove(enity);
	}

	public Employee getId(int id) {

		return entityManager.find(Employee.class, id);
	}

	public void add(Employee enity) {
		this.entityManager.persist(enity);
	}

	public Employee update(Employee enity) {
		return entityManager.merge(enity);
	}

}
