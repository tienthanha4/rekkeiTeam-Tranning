package taskBE2.DTO;

public class EmploySeachDTO {
	
	public String key;
	
	public EmploySeachDTO() {
		
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public EmploySeachDTO(String key) {
		super();
		this.key = key;
	}	
	

}
