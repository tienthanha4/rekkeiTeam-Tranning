package taskBE2.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



import taskBE2.DTO.EmploySeachDTO;
import taskBE2.Services.EmployeeService;
import taskBE2.enities.Employee;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/findAll")
	public List<Employee> findAll() {
		
		return employeeService.findAll();
	}

	@GetMapping("/search/{search}")
	public List<Employee> search(@PathVariable("search") String search) {
		EmploySeachDTO employSeachDTO = new EmploySeachDTO();

		employSeachDTO.key = search;

		return employeeService.Search(employSeachDTO);
	}

	@GetMapping("/delete/{id}")
	public List<Employee> deleteById(@PathVariable("id") int id) {
		employeeService.deleteById(id);
		return findAll();

	}

	@GetMapping("/get/{id}")
	public Employee getid(@PathVariable("id") String id) {
		int idd = Integer.parseInt(id);
		return employeeService.getId(idd);

	}

	@PutMapping("/edit/{id}")
	public List<Employee> edit(@PathVariable("id") String id, @RequestBody Employee employee) {
		int idd = Integer.parseInt(id);
		Employee employee2 = employeeService.getId(idd);
		employee2.setId(idd);
		employee2.setAge(employee.getAge());
		employee2.setCode(employee.getCode());
		employee2.setEmail(employee.getEmail());
		employee2.setName(employee.getName());
		employee2.setPhone(employee.getPhone());
		employeeService.update(employee2);
		return findAll();

	}

	@GetMapping("/getexcel")
	public void getexcel() throws IOException {

		List<Employee> employees = employeeService.findAll();

		FileOutputStream fileOutputStream = new FileOutputStream("D:\\backend\\test.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet xssfSheet = workbook.createSheet("name");
		XSSFRow row;
		XSSFCell cell;
		row = xssfSheet.createRow((short)0);
		cell = row.createCell((short)0);
		cell.setCellValue("id");
		cell = row.createCell((short)1);
		cell.setCellValue("code");
		cell = row.createCell((short)2);
		cell.setCellValue("email");
		cell = row.createCell((short)3);
		cell.setCellValue("name");
		cell = row.createCell((short)4);
		cell.setCellValue("phone");
	
		int countrow = 0;

		for (int i = 0; i < employees.size(); i++) {
			row = xssfSheet.createRow((short)++countrow);
			cell = row.createCell((short)0);
			cell.setCellValue(employees.get(i).getId());
			cell = row.createCell((short)1);
			cell.setCellValue(employees.get(i).getCode());
			cell = row.createCell((short)2);
			cell.setCellValue(employees.get(i).getEmail());
			cell = row.createCell((short)3);
			cell.setCellValue(employees.get(i).getName());
			cell = row.createCell((short)4);
			cell.setCellValue(employees.get(i).getPhone());
		
		}
		workbook.write(fileOutputStream);
		workbook.close();
		fileOutputStream.close();
	

	}

	
	
	
		
	
	

}
